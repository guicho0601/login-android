package com.lrgt.maestria.login.modelos;

import java.util.List;

/**
 * Created by Luis Ramos on 20/07/17.
 */

public class EncuestaEstudiante {

    private final String nombre, preferencia, version;
    private final boolean gusto;
    private final List<String> telefonos;
    private final int punteo, id;

    public EncuestaEstudiante(int id, String nombre, String preferencia, String version, boolean gusto, List<String> telefonos, int punteo) {
        this.id = id;
        this.nombre = nombre;
        this.preferencia = preferencia;
        this.version = version;
        this.gusto = gusto;
        this.telefonos = telefonos;
        this.punteo = punteo;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getPreferencia() {
        return preferencia;
    }

    public String getVersion() {
        return version;
    }

    public boolean isGusto() {
        return gusto;
    }

    public List<String> getTelefonos() {
        return telefonos;
    }

    public int getPunteo() {
        return punteo;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(String s : getTelefonos()){
            sb.append(s);
            sb.append(", ");
        }
        return "Nombre: " + getNombre() + "\n" +
                "Le gusta Android: " + (isGusto() ? "Sí" : "No") + "\n" +
                "Sistema que prefiere: " + getPreferencia() + "\n" +
                "Versión: " + getVersion() + "\n" +
                "Teléfonos que ha utilizado: " + sb + "\n" +
                "Punteo: " + getPunteo();
    }
}
