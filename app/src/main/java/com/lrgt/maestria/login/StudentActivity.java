package com.lrgt.maestria.login;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.Switch;

import com.lrgt.maestria.login.modelos.EncuestaEstudiante;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StudentActivity extends AppCompatActivity {

    private AdminSQLiteOpenHelper admin;
    private EditText et4;
    private Switch sw1;
    private RadioButton rb1, rb2;
    private Spinner sp1;
    private CheckBox cb1, cb2, cb3, cb4;
    private RatingBar rab1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);

        et4 = (EditText) findViewById(R.id.et4);
        sw1 = (Switch) findViewById(R.id.sw1);
        rb1 = (RadioButton) findViewById(R.id.rb1);
        rb2 = (RadioButton) findViewById(R.id.rb2);
        sp1 = (Spinner) findViewById(R.id.sp1);
        cb1 = (CheckBox) findViewById(R.id.cb1);
        cb2 = (CheckBox) findViewById(R.id.cb2);
        cb3 = (CheckBox) findViewById(R.id.cb3);
        cb4 = (CheckBox) findViewById(R.id.cb4);
        rab1 = (RatingBar) findViewById(R.id.rab1);

        List<String> versiones = new ArrayList<>();
        versiones.add("Honeycomb 3.2.6");
        versiones.add("Ice Cream Sandwich 4.0");
        versiones.add("Jelly Bean 4.1");
        versiones.add("Kit Kat 4.4");
        versiones.add("Lollipop 5.0");
        versiones.add("Marshmallow 6.0");
        versiones.add("Nougat 7.0");
        sp1.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, versiones));

        admin = new AdminSQLiteOpenHelper(this, "administracion", null, 2);

        et4.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void guardarEncuesta(View view) {
        String nombre = et4.getText().toString();
        String preferencia = rb1.isChecked() ? rb1.getText().toString() : rb2.getText().toString();
        String version = (String) sp1.getSelectedItem();
        List<String> telefonos = new ArrayList<>();
        if (cb1.isChecked())
            telefonos.add(cb1.getText().toString());
        if (cb2.isChecked())
            telefonos.add(cb2.getText().toString());
        if (cb3.isChecked())
            telefonos.add(cb3.getText().toString());
        if (cb4.isChecked())
            telefonos.add(cb4.getText().toString());
        int punteo = (int) rab1.getRating();
        boolean gusto = sw1.isChecked();
        SQLiteDatabase db = admin.getWritableDatabase();
        Cursor c = db.rawQuery("select max(id) as max from encuesta", null);
        int id = 0;
        while(c.moveToNext()) id = c.getInt(c.getColumnIndex("max"));
        ContentValues registros = new ContentValues();
        String tel = "";
        for (String t : telefonos)
            tel += t + ",";
        registros.put("id", id + 1);
        registros.put("nombre", nombre);
        registros.put("preferencia", preferencia);
        registros.put("version", version);
        registros.put("punteo", punteo);
        registros.put("gusto", gusto ? 1 : 0);
        registros.put("telefonos", tel);
        db.insert("encuesta", null, registros);
        db.close();

        et4.setText("");
        rb1.setChecked(false);
        rb2.setChecked(false);
        sp1.setSelection(0);
        cb1.setChecked(false);
        cb2.setChecked(false);
        cb3.setChecked(false);
        cb4.setChecked(false);
        rab1.setRating(0);
        sw1.setChecked(false);

        this.finish();
    }
}
