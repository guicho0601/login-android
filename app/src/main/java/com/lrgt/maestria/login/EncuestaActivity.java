package com.lrgt.maestria.login;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.lrgt.maestria.login.modelos.EncuestaEstudiante;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EncuestaActivity extends AppCompatActivity {

    private AdminSQLiteOpenHelper admin;
    private ArrayAdapter<String> adapter;
    private List<String> estudiantes;
    private List<EncuestaEstudiante> encuestaEstudianteList;
    private int pos = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encuesta);

        admin = new AdminSQLiteOpenHelper(this, "administracion", null, 2);
        ListView lv2 = (ListView) findViewById(R.id.lv2);
        estudiantes = new ArrayList<>();
        encuestaEstudianteList = new ArrayList<>();
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, estudiantes);
        lv2.setAdapter(adapter);
        lv2.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                pos = i;
                createSingleListDialog().show();
                return false;
            }
        });
        this.mostrarEncuestas();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.mostrarEncuestas();
    }

    public void mostrarEncuestas(){
        String query="select * from encuesta";
        SQLiteDatabase db = admin.getReadableDatabase();
        estudiantes.clear();
        Cursor c = db.rawQuery(query,null);
        while(c.moveToNext()){
            List<String> telefonos = Arrays.asList(c.getString(c.getColumnIndex("telefonos")).split(","));
            EncuestaEstudiante encuestaEstudiante = new EncuestaEstudiante(
                    c.getInt(c.getColumnIndex("id")),
                    c.getString(c.getColumnIndex("nombre")),
                    c.getString(c.getColumnIndex("preferencia")),
                    c.getString(c.getColumnIndex("version")),
                    c.getInt(c.getColumnIndex("gusto")) == 1,
                    telefonos,
                    c.getInt(c.getColumnIndex("punteo"))
            );
            encuestaEstudianteList.add(encuestaEstudiante);
            estudiantes.add(encuestaEstudiante.toString());
        }
        adapter.notifyDataSetChanged();
    }

    public void nuevaEncuesta(View view){
        startActivity(new Intent(this, StudentActivity.class));
    }

    private void eliminar(){
        EncuestaEstudiante e = encuestaEstudianteList.get(pos);
        SQLiteDatabase db = admin.getReadableDatabase();
        db.delete("encuesta", "id=" + e.getId(), null);
        db.close();
        encuestaEstudianteList.remove(pos);
        estudiantes.remove(pos);
        pos = -1;
        mostrarEncuestas();

    }

    public AlertDialog createSingleListDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        final CharSequence[] items = new CharSequence[1];

        items[0] = "Eliminar";

        builder.setTitle("Opciones")
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0){
                            eliminar();
                        }
                    }
                });

        return builder.create();
    }
}
