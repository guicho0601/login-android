package com.lrgt.maestria.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText et1, et2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et1 = (EditText) findViewById(R.id.et1);
        et2 = (EditText) findViewById(R.id.et2);
        et1.setText("ing@catedratico.gt");
        et2.setText("posgrado");
    }

    public void acceder(View view){
        Intent intent;
        String correo = et1.getText().toString();
        String pass = et2.getText().toString();
        if(correo.endsWith("@catedratico.gt") && pass.equals("posgrado")){
            intent = new Intent(this, TeacherActivity.class);
        }else{
            intent = new Intent(this, EncuestaActivity.class);
        }
        startActivity(intent);
    }
}
