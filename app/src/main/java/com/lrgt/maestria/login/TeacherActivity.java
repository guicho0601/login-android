package com.lrgt.maestria.login;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class TeacherActivity extends AppCompatActivity {

    private EditText et3;
    private ListView lv1;
    private List<String> list;
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher);

        et3 = (EditText) findViewById(R.id.et3);
        lv1 = (ListView) findViewById(R.id.lv1);
        list = new ArrayList<>();
        list.add("Pepe");
        list.add("Luis");
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);
        lv1.setAdapter(adapter);
    }

    public void agregarAlumno(View view){
        String nombre = et3.getText().toString();
        if(!nombre.equals("")){
            list.add(nombre);
            adapter.notifyDataSetChanged();
            et3.setText("");
        }
    }
}
